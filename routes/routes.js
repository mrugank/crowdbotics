// Dependencies
const mongoose = require('mongoose');
// Model
const Questions = mongoose.model('Question');
const Result = mongoose.model('Result');


exports.getQuestions = async (req, res) => {
    try {
        console.log("here");
        let items = await Questions.aggregate({$sample: {size: 10}});
        res.json({questions: items});
    } catch (err) {
        res.json({err: err});
    }
};

exports.save = async (req, res) => {
    let newResult = new Result({
        name: req.body.name,
        score: req.body.score,
        date: new Date()
    });
    try {
        let result = await Result.registerResult(newResult);
        res.json({success: true, msg: 'Score Registered!'});
    } catch (err) {
        res.json({success: false, msg: 'Score Registration Failed! ' + err})
    }
};

exports.getScores = async (req, res) => {
    try {
        let items = await Result.find();
        res.json({results: items});
    } catch (err) {
        res.json({err: err});
    }
};

