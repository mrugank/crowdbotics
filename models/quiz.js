// Dependencies
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
    id: Number,
    question: String,
    options: Array,
    answer: String
});

const ResultSchema = new Schema({
    name: String,
    score: Number,
    date: Date
});

const Result = module.exports = mongoose.model('Result', ResultSchema);
mongoose.model('Question', QuestionSchema);

// Register Quiz Result
module.exports.registerResult = function (newResult, callback) {
    newResult.save(callback)
};


