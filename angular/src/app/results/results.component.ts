import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {NgForm} from '@angular/forms';
// Service
import { QuizService } from '../quiz/quiz.service';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  name;
  quizScore: number = null;
  @Output() scoreSend = new EventEmitter<number>();
  profile: any;

  constructor(
    public quizService: QuizService,
    public router: Router,
  ) { }

  ngOnInit() {
    // Get Current Score
    this.quizScore = this.quizService.score;
    this.profile = JSON.parse(localStorage.getItem('profile'));
  }

  registerScore(form: NgForm) {

    // result object
    const result = {
      name: this.name,
      score: this.quizScore,
      date: new Date()
    }

    // result object registration
    this.quizService.registerResult(result).subscribe(data => {
        if (data.success) {
          this.router.navigate(['/leaderboard']);
        }
      },
      err => {
        console.log(err);
        return false;
      });
  }

  // Send Score to Quiz Component (parent)
  sendScore() {
    this.scoreSend.emit(this.quizScore);
  }

  ngOnDestroy() {
    this.quizService.quizScore(null);
    this.quizService.quizDone(false);
  }

}
