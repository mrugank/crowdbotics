import { Component, OnInit } from '@angular/core';

// Service
import { QuizService } from '../quiz/quiz.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {

  quiz: any;
  questionIndex: number = 1;
  selectedOptions: Array<string> = [];
  activeOptions = document.getElementsByClassName('active');
  quizScore: number = 0;
  secLeft: number = this.quizService.time;

  constructor(public quizService: QuizService) { }

  // selected options
  selected(elem: any) {
    elem.classList.toggle('active');
    this.selectedOptions.length = 0;
    for (var i = 0; i < this.activeOptions.length; i++) {
      this.selectedOptions.push(this.activeOptions[i].innerHTML);
    }
  }

  // next question function
  nextQuestion() {
    if (this.questionIndex <= this.quiz.length) {
      for (let i = this.questionIndex - 1; i < this.questionIndex; i++) {
        var selectedAnswers = String(this.selectedOptions);
        var correctAnswer = String(this.quiz[i].options[this.quiz[i].answer]);
        console.log(selectedAnswers,correctAnswer)
        if (selectedAnswers === correctAnswer) {
          this.quizScore++;
        }
        if (this.questionIndex === this.quiz.length) {
          this.calculateScore();
        }
      }
      this.questionIndex++;
      this.selectedOptions.length = 0;
    }
  }

  calculateScore() {
    this.quizScore = (this.quizScore / this.quiz.length) * 100;
    this.quizService.quizDone(true);
    this.quizService.quizScore(this.quizScore);
  }

  ngOnInit() {
    // get quiz data
    this.quizService.getQuizData().subscribe(quiz => {
        this.quiz = quiz.questions;
      },
      // error
      err => {
        console.log(err);
        return false;
      }
    );
  }

  ngDoCheck() {
    // get timer value
    this.secLeft = this.quizService.time;
    if (this.secLeft === 0) {
      this.quizService.quizScore(this.quizScore * 10)
    }
  }


}
