import {Component, OnInit} from '@angular/core';
// Service
import {QuizService} from '../quiz/quiz.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  highScores: any;

  constructor(public quizService: QuizService) {
  }

  ngOnInit() {

    // Get High Scores
    this.quizService.getHighScores().subscribe(highScores => {

        // Sort Scores Descending
        highScores.results.sort(function (score1, score2) {
          if (score1.score > score2.score) {
            return -1;
          } else if (score1.score < score2.score) {
            return 1;
          } else {
            return 0;
          }
        });

        this.highScores = highScores.results;

      },
      // error
      err => {
        console.log(err);
        return false;
      }
    );

  }


}
