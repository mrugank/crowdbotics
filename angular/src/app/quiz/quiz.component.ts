import { Component, DoCheck } from '@angular/core';

// Service
import { QuizService } from './quiz.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent {

  score: number = null;
  isQuizDone = false;

  constructor(public quizService: QuizService) { }

  // handleScore() triggered by sendScore Event, sent from Results Component
  handleScore(data: number) {
    this.score = data;
  }
  // Checks if Quiz is Finished
  ngDoCheck() {
    if (this.quizService.isQuizDone === true) {
      this.score = this.quizService.score;
      this.isQuizDone = this.quizService.isQuizDone;
    }
  }
}
