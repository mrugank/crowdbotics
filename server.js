// Dependensies
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const publicUrl = 'public/index.html';
const morgan = require('morgan');
const bluebird = require('bluebird');

// Models
require('./models/quiz');

// Routes
const routeQuiz = require('./routes/routes');

// Config
const dbConfig = require('./config/db');

// Server
const app = express();
const port = process.env.PORT || 3000;

// DB Connection
mongoose.Promise = bluebird;
mongoose.connect(dbConfig.database, {
    useMongoClient: true,
});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function callback() {
    console.log("Connection with database succeeded.");
});


// CORS
app.use(cors());
app.use(bodyParser.json());
app.use(morgan('dev'));

app.use(express.static(path.join(__dirname, 'public')));

app.listen(port, function () {
    console.log('Server started on port: ' + port)
});

// API Routes
app.get('/api/quiz' , routeQuiz.getQuestions);
app.get('/api/scores/high-scores', routeQuiz.getScores);
app.post('/api/scores/save',routeQuiz.save);

// Server Routes
app.get('/', (req, res) => {
    res.send('Error: Cannot reach for public files!');
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, publicUrl));
});
